from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import login
import ToDoTask
from ToDoTask.todo import ToDoUpdateView
from ToDoTask.tasklist import ToDoTaskListView


urlpatterns = [
    url(r'^$', login, {'template_name': 'login.html'}, name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'login/', login, {'template_name': 'login.html'}, name='login'),
    url(r'^add-todo/', ToDoTask.todo.ToDoCreateView.as_view()),
    url(r'^edit/(?P<pk>\d+)/', ToDoTask.todo.ToDoUpdateView.as_view(), name='task-update'),
    url(r'^delete/(?P<pk>\d+)/', ToDoTask.todo.ToDoDeleteView.as_view(), name='task-delete'),
    url(r'^home/$', ToDoTask.tasklist.ToDoTaskListView.as_view(), name='task-list'),

]