from django.contrib.auth.models import User
from django.db import models
from datetime import datetime


# ToDo - Employee CRUD
# ToDo - If user is admin then he can create, update and assign tasks, if user is not admin he can only change task status
# ToDo - Put every style in CSS

class ToDoTask(models.Model):
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    start_date = models.DateTimeField(editable=False, default=datetime.now())
    user = models.ForeignKey(User)
    completed = models.BooleanField(default=False)
    employee = models.ForeignKey(User, related_name='employee')
