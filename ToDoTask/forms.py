from django import forms
from ToDoTask.models import ToDoTask

__author__ = 'Gresi'


class ToDoForm(forms.ModelForm):
    class Meta:
        model = ToDoTask
        exclude = ["start_date", "user", "employee"]
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.TextInput(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'})
        }
