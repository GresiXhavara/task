from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.views.generic import View
from django.views.generic.list import ListView
from ToDoTask.models import ToDoTask
from task import settings


class ToDoTaskListView(ListView):
    model = ToDoTask
    template_name = "tasklist.html"

    def get_context_data(self, **kwargs):
        context = super(ToDoTaskListView, self).get_context_data(**kwargs)
        return context

    def get_queryset(self):
        if self.request.user.is_staff:
            return ToDoTask.objects.all()
        else:
            return ToDoTask.objects.filter(employee=self.request.user)


