from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic.edit import CreateView, ModelFormMixin
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from ToDoTask.forms import ToDoForm
from ToDoTask.models import ToDoTask


class ToDoCreateView(CreateView):
    model = ToDoTask
    object = None
    form_class = ToDoForm
    template_name = 'add_todo.html'
    success_url = reverse_lazy('task-list')

    def get_initial(self):
        self.initial['employee'] = self.request.user
        return self.initial.copy()

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.employee = self.request.user
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class ToDoUpdateView(UpdateView):
    model = ToDoTask
    form_class = ToDoForm
    template_name = 'update.html'
    success_url = reverse_lazy('task-list')


class ToDoDeleteView(DeleteView):
    model = ToDoTask
    template_name = 'delete.html'
    success_url = reverse_lazy("task-list")
